<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aspect extends Model
{
	protected $table = 'aspects';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	    'name',
	];

    public function qualifications(){
        return $this->hasMany('App\Qualification');
	}
	public function scopeSearch($query, $name){
        return $query->where('name', 'LIKE', "%$name%");
    }
}
