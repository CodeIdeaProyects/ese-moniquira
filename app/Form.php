<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $table = 'forms';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'user_id',' type_id', 'name', 'state', 'version', 'code', 'document'
    ];

    public function category(){
    	return $this->belongsTo('App\Category');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function type(){
    	return $this->belongsTo('App\Type');
    }
    
    public function scopeSearch($query, $name){
        return $query->where('name', 'LIKE', "%$name%");
    }

    public function getFormat(){
        $array = explode('.', $this->document);
        $format = end($array);
        return $format;
    }
}
