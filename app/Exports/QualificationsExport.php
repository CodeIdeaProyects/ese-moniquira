<?php

namespace App\Exports;

use App\Qualification;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;


class QualificationsExport implements FromCollection,WithHeadings
{
	use Exportable;
    protected $qualifications;
    public function __construct($qualifications = null)
    {
        $this->qualifications = $qualifications;
    }
public function headings(): array
        {
            return [
                'Nombre usuario Calificado',
                'Apellido usuario Calificado',
                'Nombre usuario Calificador',
                'Apellido usuario Calificador',
                'Justificacion',
                'Valor',
                'Principio',
                'Documento calificador',
                'Fecha de voto' 
            ];
        }

    public function collection()
    {
        return $this->qualifications;
    }
}
