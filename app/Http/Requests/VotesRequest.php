<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VotesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_concurso'       =>  'required|string|min:3|max:240',
            'fecha_inicio'          =>  'required|date',
            'fecha_finalizacion'    =>  'required|date|after:fecha_inicio',
            'votacion_maxima'       =>  'required|numeric|min:1|max:10',
        ];
    }
}
