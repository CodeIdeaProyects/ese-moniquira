<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':{
                return [
                    'nombre'                    =>  'required|min:3|max:120|string',
                    'apellido'                  =>  'required|min:3|max:120|string',
                    'email'                     =>  'required|email|unique:users',
                    'tipo_identificacion'       =>  'required',
                    'funciones'                 =>  'required',
                    'numero_identificacion'     =>  'required|numeric',
                    'genero'                    =>  'required',
                    'rol'                       =>  'required',
                    'fecha_nacimiento'          =>  'required|date',
                    'contrasena'                =>  'required|min:8|string|confirmed',
                ];
            }
            case 'PUT':
            case 'PATCH':{
                return [
                    'nombre'                    =>  'required|min:3|max:120|string',
                    'apellido'                  =>  'required|min:3|max:120|string',
                    'tipo_identificacion'       =>  'required',
                    'funciones'                 =>  'required',
                    'numero_identificacion'     =>  'required|numeric',
                    'genero'                    =>  'required',
                    'rol'                       =>  'required',
                    'fecha_nacimiento'          =>  'required|date',
                ];    
            }
        }
    }
}
