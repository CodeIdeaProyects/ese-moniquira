<?php

namespace App\Http\View\Composers;

use Illuminate\Contracts\View\View;
use App\Category;

class AsideComposer{

	public function compose(View $view)
	{
		$categories = Category::where('name', 'Novedades')->orderBy('name', 'ASC')->get();

		$view->with('categories', $categories);
	}

}