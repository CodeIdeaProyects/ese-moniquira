<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use App\Http\Requests\BannersRequest;

class BannersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::orderBy('id', 'DESC')->paginate(5);

        return view('admin.banners.index')->with('banners', $banners);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannersRequest $request)
    {
        if($request->file('imagen')){
            $banner = new Banner();
            $file = $request->file('imagen');
            $name_file = $file->getClientOriginalName();
            $name_file = str_replace(' ', '', $name_file);
            $name = "banner-" . time() . $name_file;
            $ruta = public_path() . '/storage/banners/';
            $file->move($ruta, $name);
            $banner->image = $name;
            
            if(!is_null($request->texto)){
                $banner->text = $request->texto;
            }
            if(!is_null($request->link)){
                $banner->link = $request->link;   
            }

            try{
                $banner->save();

                flash('Banner creado exitosamente')->success()->important();
            }catch(Exception $e){
                flash('Error al crear el banner')->error()->important();
            }
        }else{
            flash('Error al crear el banner')->error()->important();
        }
        
        return redirect()->route('banners.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::find($id);

        return view('admin.banners.edit')->with('banner', $banner);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BannersRequest $request, $id)
    {
        $banner = Banner::find($id);
        
        if(!is_null($request->texto)){
            $banner->text = $request->texto;
        }else{
            $banner->text = null;
        }
        if(!is_null($request->link)){
            $banner->link = $request->link;   
        }else{
            $banner->link = null;
        }

        if($request->file('imagen')){
            if(!is_null($banner->image)){
                unlink(public_path() . "/storage/banners/" . $banner->image);
            }
            $file = $request->file('imagen');
            $name_file = $file->getClientOriginalName();
            $name_file = str_replace(' ', '', $name_file);
            $name = "banner-" . time() . $name_file;
            $ruta = public_path() . '/storage/banners/';
            $file->move($ruta, $name);
            $banner->image = $name;    
        }
        
        try{
            $banner->save();

            flash('Banner creado exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al crear el banner')->error()->important();
        }
        
        return redirect()->route('banners.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::find($id);

        if(is_null($banner)){
            flash('El banner ya fue eliminado anteriormente')->error()->important();
        }else{
            try{
                if(!is_null($banner->image)){
                    unlink(public_path() . "/storage/banners/" . $banner->image);
                }

                $banner->delete();

                flash('Banner eliminado correctamente')->success()->important();
            }catch(Exception $e){
                flash('Error al eliminar el banner')->error()->important();
            }
        }

        return redirect()->route('banners.index');
    }
}
