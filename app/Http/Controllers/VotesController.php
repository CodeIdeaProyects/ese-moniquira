<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vote;
use App\User;
use App\Exports\UsersExport;
use App\Exports\QualificationsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\VotesRequest;
use App\Support\Collection;
use Illuminate\Support\Facades\DB;

class VotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $votes = Vote::search($request->name)->orderBy('id', 'ASC')->paginate(20);
        $search = $request->name;

        return view('admin.votes.index')->with('votes', $votes)->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.votes.create');   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VotesRequest $request)
    {
        $vote = new Vote();
        $vote->name = $request->nombre_concurso;
        $vote->date_start = $request->fecha_inicio;
        $vote->date_end = $request->fecha_finalizacion;
        $vote->maximum = $request->votacion_maxima;
        
        try{
            $vote->save();

            flash('Votaciones creadas exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al crear las votaciones')->error()->important();
        }

        return redirect()->route('votes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vote = Vote::find($id);

        return view('admin.votes.edit')->with('vote', $vote);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VotesRequest $request, $id)
    {
        $vote = Vote::find($id);
        $vote->name = $request->nombre_concurso;
        $vote->date_start = $request->fecha_inicio;
        $vote->date_end = $request->fecha_finalizacion;
        $vote->maximum = $request->votacion_maxima;
        
        try{
            $vote->save();

            flash('Votación editada exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al editar las votaciones')->error()->important();
        }

        return redirect()->route('votes.index');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vote = Vote::find($id);

        if($vote->qualifications->count() > 0){
            flash('Error al eliminar las votaciones, tienen calificaciones asignadas')->error()->important();
        }else{
            try{
                $vote->delete();

                flash('Votaciones eliminadas correctamente')->success()->important();
            }catch(Exception $e){
                flash('Error al eliminar las votaciones')->error()->important();
            }
        }

        return redirect()->route('votes.index');
    }

    public function reports($id){
             $queryQualifications= DB::select(
            'SELECT u.name AS nombrecali,u.lastname AS apellidocali,us.name AS nombre,us.lastname AS apellido,q.justify,a.name,b.name AS nombreP,us.identification_number as documento,q.created_at AS fechaCreacion FROM qualifications q JOIN users u ON q.user_id_qualified=u.id JOIN users us ON q.user_id=us.id JOIN aspects a ON a.id=q.aspect_id JOIN beginings b ON q.begining_id=b.id where q.vote_id='.$id.'');
        $qualifications = (new Collection($queryQualifications));
        
        return (new QualificationsExport($qualifications))->download('votaciones.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }
}