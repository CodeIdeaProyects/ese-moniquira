<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aspect;
use App\Http\Requests\AspectsRequest;

class AspectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $aspects = Aspect::search($request->name)->orderBy('id', 'ASC')->paginate(20);
        $search = $request->name;

        return view('admin.aspects.index')->with('aspects', $aspects)->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.aspects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AspectsRequest $request)
    {
        $aspect = new Aspect();
        $aspect->name = $request->nombre;

        try{
            $aspect->save();

            flash('Valor creado exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al crear el valor')->error()->important();
        }

        return redirect()->route('aspects.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aspect = Aspect::find($id);

        return view('admin.aspects.edit')->with('aspect', $aspect);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AspectsRequest $request, $id)
    {
        $aspect = Aspect::find($id);
        $aspect->name = $request->nombre;

    try{
        $aspect->save();

        flash('Valor actualizado exitosamente')->success()->important();
    }catch(Exception $e){
        flash('Error al actualizar el valor')->error()->important();
    }

    return redirect()->route('aspects.index');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aspect = Aspect::find($id);

        if($aspect->qualifications->count() > 0){
            flash('Error al eliminar el valor, tiene información relacionada')->error()->important();
        }else{
            try{
                $aspect->delete();

                flash('Valor eliminado correctamente')->success()->important();
            }catch(Exception $e){
                flash('Error al eliminar el valor')->error()->important();
            }
        }

        return redirect()->route('aspects.index');
    }
}

