<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Type;
use App\Form;
use App\Http\Requests\CategoriesRequest;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::search($request->name)->orderBy('id', 'ASC')->paginate(20);
        $search = $request->name;

        return view('admin.categories.index')->with('categories', $categories)->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('category_id', null)->orderBy('name', 'ASC')->pluck('name', 'id');

        return view('admin.categories.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesRequest $request)
   {
        $category = new Category();
        $category->name = $request->nombre;
        if($request->tipo_proceso === 'Hija'){
            $category_f = Category::find($request->macroproceso);
            $category->category()->associate($category_f);
            $category->code = $request->codificacion;
        }else{
            $category->code = null;
        }
        try{
            $category->save();

            flash('Proceso creado exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al crear el proceso')->error()->important();
        }

        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $category = Category::where('slug', $slug)->firstOrFail();
        $categories = Category::where('category_id', null)->orderBy('name', 'ASC')->get();
        $colors = array('#477E00', '#5601E2', '#E5A900', '#015277', '#D9000C', '#C95200');
        $medico = array("img/medico3.png", "img/medico2.png", "img/medico1.png");

        return view('users.categories.show')->with('category', $category)->with('categories', $categories)->with('colors', $colors)->with('medico', $medico);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $categories = Category::where('category_id', null)->where('id','!=',$category->id)->orderBy('name', 'ASC')->pluck('name', 'id');

        return view('admin.categories.edit')->with('category', $category)->with('categories',$categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriesRequest $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->nombre;
        if($request->tipo_proceso === 'Hija'){
            $category_f = Category::find($request->macroproceso);
            $category->category()->associate($category_f);
            $category->code = $request->codificacion;
        }else{
            $category->category_id = null;
            $category->code = null;
        }
        try{
            $category->save();

            flash('Proceso actualizado exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al actualizar el proceso')->error()->important();
        }

        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if($category->categories->count() > 0){
            flash('Error al eliminar el proceso, tiene subprocesos asociados')->error()->important();
        }else{
            try{
                if(!is_null($category->image)){
                    unlink(public_path() . "/storage/categories/" . $category->image);
                }

                $category->delete();

                flash('Proceso eliminado correctamente')->success()->important();
            }catch(Exception $e){
                flash('Error al eliminar el proceso')->error()->important();
            }
        }

        return redirect()->route('categories.index');
    }

    public function getProcessById(Request $request, $id){
        $category = Category::find($id);
        $categories = $category->categories;
        return response()->json($categories);
    }

    public function showSubprocess($macroproceso, $subproceso){
        $macro = Category::where('slug', $macroproceso)->firstOrFail();
        $sub = Category::where('slug', $subproceso)->firstOrFail();
        $categories = Category::where('category_id', null)->orderBy('name', 'ASC')->get();
        $types = Type::orderBy('name', 'ASC')->get();
        $documents = $sub->forms->where('category_id', $sub->id);
        $medico = array("img/medico3.png", "img/medico2.png", "img/medico1.png");

        return view('users.categories.subprocess')->with('macro', $macro)->with('sub', $sub)->with('categories', $categories)->with('types', $types)->with('medico', $medico);
    }

    public function showPDF($id){
        $form = Form::find($id);

        return view('users.categories.pdfview')->with('form', $form);
    }
}
