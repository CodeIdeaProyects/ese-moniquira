<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Form;
use App\Category;
use App\Type;
use App\User;
use App\Http\Requests\FormsRequest;
use App\Exports\FormsExport;
use Illuminate\Support\Facades\DB;
use App\Support\Collection;


class FormsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $forms = Form::search($request->name)->orderBy('id', 'ASC')->
        where('state', 'active')->paginate(20);
        $search = $request->name;

        return view('admin.forms.index')->with('forms', $forms)->with('search', $search);
    }

    public function scopeF(Request $request)
    {
        $forms = Form::search($request->name)->where('state', 'Active')->orderBy('id', 'ASC')->paginate(20);
        $categories = Category::where('category_id', null)->orderBy('name', 'ASC')->get();
        $types = Type::orderBy('name', 'ASC')->get();
        $medico = array("img/medico3.png", "img/medico2.png", "img/medico1.png");

        return view('users.categories.search')->with('forms', $forms)->with('categories', $categories)->with('types', $types)->with('medico', $medico);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('category_id', null)->orderBy('name', 'DESC')->pluck('name', 'id');
        $types = Type::orderBy('name', 'DESC')->pluck('name', 'id');

        return view('admin.forms.create')->with('categories', $categories)->with('types', $types);  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormsRequest $request)
    {
        $code = '';
        $form = new Form();
        $user = Auth::user();
        $category = Category::find($request->subproceso);
        $type = Type::find($request->tipo_formulario);
        $form->name = $request->nombre;
        $form->user()->associate($user);
        $form->category()->associate($category);
        $form->type()->associate($type);
        if($request->codificacion === 'Automatico'){
            $code .= strtoupper($category->code);
            $code .= '-';
            $code .= strtoupper($type->code);
            $code .= '-';

            $form_last = Form::where('code', 'LIKE', "$code%")->orderBy('id', 'ASC')->get();
            if($form_last->count() > 0){
                $max = 1;
                foreach ($form_last as $form_l) {
                    $aux = explode("-", $form_l->code);
                    $aux = end($aux);
                    if(intval($aux) >= $max){
                        $max = intval($aux);
                    }
                }
                $number = $max;
                $number = intval($number) + 1;
                $code .= $number;
            }else{
                $code .= '1';
            }
            $form->code = $code;
        }else{
            $form->code = $request->codigo;
        }
        $form->type_cod = $request->codificacion;
        $form->version = 1;
        $form->state = 'Active';

        if($request->file('evidencia')){
            $file = $request->file('evidencia');
            $name = $file->getClientOriginalName();
            $ruta = public_path() . '/storage/forms/';
            $file->move($ruta, $name);
            $form->document = $name;
        }

        try{
            $form->save();

            flash('Formulario creado exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al crear el formulario')->error()->important();
        }

        return redirect()->route('forms.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form = Form::find($id);
        $categories = Category::where('category_id', '<>', null)->orderBy('name', 'DESC')->pluck('name', 'id');
        $types = Type::orderBy('name', 'DESC')->pluck('name', 'id');

        return view('admin.forms.edit')->with('form', $form)->with('categories',$categories)->with('types',$types);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormsRequest $request, $id)
    {
        $form = Form::find($id);
        $form_new = new Form();
        $user = Auth::user();
        $category = Category::find($form->category_id);
        $type = Type::find($form->type_id);
        $form_new->name = $request->nombre;
        $form_new->user()->associate($user);
        $form_new->category()->associate($category);
        $form_new->type()->associate($type);
        $form_new->observation = $request->observacion;
        $form_new->version = $form->version + 1;
        $form_new->type_cod = $form->type_cod;

        if($form->type_cod === 'Manual'){
            $form_new->code = $request->codigo;
        }else{
            $form_new->code = $form->code;
        }
        if($request->file('evidencia')){
            $file = $request->file('evidencia');
            $name = $file->getClientOriginalName();
            $ruta = public_path() . '/storage/forms/';
            $file->move($ruta, $name);
            $form_new->document = $name;
        }else{
            $form_new->document = $form->document;
        }
        $form->state = 'Inactive';
        $form_new->state = 'Active';

        try{
            $form->save();
            $form_new->save();

            flash('Documento creado exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al crear el documento')->error()->important();
        }

        return redirect()->route('forms.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = Form::find($id);

        try{
            if(!is_null($form->document)){
                unlink(public_path() . "/storage/forms/" . $form->document);
            }

            $form->delete();

            flash('Formulario eliminado correctamente')->success()->important();
        }catch(Exception $e){
            flash('Error al eliminar el formulario')->error()->important();
        } 
           
        return redirect()->route('forms.index');
    }

    public function history($id){
        $form = Form::find($id);
        $forms = Form::where('code', $form->code)->orderBy('created_at', 'DESC')->get();

        return view('admin.forms.history')->with('forms', $forms)->with('form', $form);
    }
    
    public function reports($id){
       $form = Form::find($id);
       $idf=$form->code;
       $queryForms=DB::select('SELECT f.name AS nameF,c.name As nameC,ca.name as nameCa,u.name As nameU,t.name AS namet,f.version,f.code,f.observation,f.created_at As fecha 
        from forms f join categories c ON f.category_id = c.id 
        join types t ON f.type_id = t.id join users u ON f.user_id = u.id 
        join categories ca on ca.id=c.category_id
        where f.code='.'"'.$idf.'"'.'
        ');
       $forms = (new Collection($queryForms));
       return (new FormsExport($forms))->download('formularioHistorial.xlsx', \Maatwebsite\Excel\Excel::XLSX);   
    }

    public function reportsDocuments(){
       $queryDoc = DB::select('SELECT f.name AS nameF,c.name As nameC,ca.name as nameCa,u.name As nameU,t.name AS namet,f.version,f.code,f.observation,f.created_at As fecha 
        from forms f join categories c ON f.category_id = c.id 
        join types t ON f.type_id = t.id join users u ON f.user_id = u.id 
        join categories ca on ca.id=c.category_id where f.state = "Active"
        ');
       $forms = (new Collection($queryDoc));
       return (new FormsExport($forms))->download('formularios.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }
}
