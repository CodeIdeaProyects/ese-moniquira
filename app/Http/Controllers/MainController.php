<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use App\Vote;
use Mail;
use Laracasts\Flash\Flash;
use File;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::orderBy('created_at', 'DESC')->get();
        $vote = Vote::orderBy('id', 'ASC')->get()->last();
        
        return view('index')->with('banners', $banners)->with('vote', $vote);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function mail(Request $request){
        $asunto = "Reporte de seguridad del paciente";
        Mail::send('mailers.contact', $request->all(),function($msj) use ($request, $asunto){
            $msj->subject((String)$asunto);
            $msj->to('siscesaroal@gmail.com');
        });
        Flash::success('<i class="fas fa-location-arrow"></i> ¡Mensaje enviado exitosamente!')->important();
        
        return back(); 
    }

    public function we(){
        return view('we');
    }

    public function news($site){
        switch ($site) {
            case 'tecnovigilancia':
                return view('users.novelties.techno');
                break;
            case 'farmacovigilancia':
                return view('users.novelties.pharma');
                break;
            case 'hemovigilancia':
                return view('users.novelties.pharma');
                break;
            case 'reactivo-vigilancia':
                return view('users.novelties.react');
                break;
            case 'control-indicadores':
                return view('users.novelties.pharma');
                break;
            case 'derechos-deberes':
                return view('users.novelties.pharma');
                break;
            case 'gestion-riesgo':
                return view('users.novelties.pharma');
                break;
            default:
                # code...
                break;
        }
    }

    public function backup(){
        $ruta = public_path() . '/storage/backups/comp-backup-database-' . getdate()['year'] . '-' .getdate()['mon'] . '-' . getdate()['mday'] . '.tar.gz';
        $headers = ["Content-Type" => "application/gzip"];

        return response()->download($ruta, 'base_de_datos.tar.gz', $headers);
    }
}
