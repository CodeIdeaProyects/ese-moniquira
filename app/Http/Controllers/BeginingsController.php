<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Begining;
use App\Http\Requests\BeginingsRequest;

class BeginingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $beginings = Begining::search($request->name)->orderBy('id', 'ASC')->paginate(20);
        $search = $request->name;

        return view('admin.beginings.index')->with('beginings', $beginings)->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.beginings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BeginingsRequest $request)
    {
        $begining = new Begining();
        $begining->name = $request->nombre;

        try{
            $begining->save();

            flash('Principio creado exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al crear el Principio')->error()->important();
        }

        return redirect()->route('beginings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $begining = Begining::find($id);

        return view('admin.beginings.edit')->with('begining', $begining);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BeginingsRequest $request, $id)
    {
            
        $begining = Begining::find($id);
        $begining->name = $request->nombre;

    try{
        $begining->save();

        flash('Principio actualizado exitosamente')->success()->important();
    }catch(Exception $e){
        flash('Error al actualizar el Principio')->error()->important();
    }

    return redirect()->route('beginings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
     {
        $begining = Begining::find($id);

        if($begining->qualifications->count() > 0){
            flash('Error al eliminar el Principio, tiene información relacionada')->error()->important();
        }else{
            try{
                $begining->delete();

                flash('Principio eliminado correctamente')->success()->important();
            }catch(Exception $e){
                flash('Error al eliminar el Principio')->error()->important();
            }
        }

        return redirect()->route('beginings.index');
    }
}
