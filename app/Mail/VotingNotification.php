<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VotingNotification extends Mailable
{
    use Queueable, SerializesModels;
    public $qualification;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($qualification)
    {
        $this->$qualification = $qualification;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('siscesaroal@gmail.com')
                ->subject('¡Felicidades! Fuiste seleccionado por un compañero')
                ->view('mailers.VotingNotification');
    }
}
