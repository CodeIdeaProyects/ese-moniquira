<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qualifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('user_id_qualified');
            $table->unsignedBigInteger('aspect_id');
            $table->unsignedBigInteger('begining_id');
            $table->unsignedBigInteger('vote_id');
            $table->string('justify', 5000);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('Cascade');
            $table->foreign('user_id_qualified')->references('id')->on('users')->onDelete('Cascade');
            $table->foreign('aspect_id')->references('id')->on('aspects')->onDelete('Cascade');
            $table->foreign('begining_id')->references('id')->on('beginings')->onDelete('Cascade');
            $table->foreign('vote_id')->references('id')->on('votes')->onDelete('Cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qualifications');
    }
}
