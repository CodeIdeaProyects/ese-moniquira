<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['name' => 'Humanización']);
        Category::create(['name' => 'Seguridad del paciente']);
        Category::create(['name' => 'Direccionamiento estratégico']);
        Category::create(['name' => 'Gestión de calidad']);
        Category::create(['name' => 'Unidades funcionales']);
        Category::create(['name' => 'Gestión del riesgo']);
        Category::create(['name' => 'Gestión de tecnología']);
        Category::create(['name' => 'Gestión integral al usuario']);
        Category::create(['name' => 'Gestión del ciclo economico']);
        Category::create(['name' => 'Gestión de recursos físicos']);
        Category::create(['name' => 'Gestión financiera']);
        Category::create(['name' => 'Gestión administrativa']);
        Category::create(['name' => 'Cultura de mejoramiento']);
        Category::create(['name' => 'Responsabilidad social']);
        Category::create(['name' => 'Evaluación y control']);
    }
}
