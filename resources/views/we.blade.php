@extends('layouts.main')

@section('menu')
    <div class="d-none">
        {!! $site = 'ext' !!}
    </div>
@endsection

@section('content')
    <div class="main-container">
        <div class="row row-we">
            <div class="col-md-6">
                <img src="{{ asset('img/mision.jpg') }}" class="w-100">
            </div>
            <div class="col-md-6">
                <div class="title-we">
                    Misión
                    <img src="{{ asset('img/logo_negro.png') }}">
                </div>
                <div class="text-we">
                    Somos una institución prestadora del servicio de salud de baja, mediana y alta complejidad líder de
                    la región, que articula acciones preventivas comunitarias e individuales para generar resultados en
                    salud, con un crecimiento y desarrollo económico sostenible, que fundamenta la prestación de los
                    servicios en la humanización, calidad, oportunidad, eficiencia y eficacia, siendo centro de
                    referencia para la región de Ricaurte y los municipios del sur del departamento de Santander, con un
                    talento humano con profundos valores de honestidad, transparencia, pulcritud y comportamiento ético
                    y responsabilidad social, con un talento humano comprometido, de la más alta calidad técnica y
                    científica, en una infraestructura física moderna amigable con el medio ambiente, con equipos y
                    tecnología de punta, con procesos y procedimientos rigurosos que nos permiten garantizar la
                    excelencia en la prestación del servicio.
                </div>
            </div>
        </div>
        <div class="row row-we">
            <div class="col-md-6">
                <div class="title-we title-vis">
                    <img src="{{ asset('img/logo_negro.png') }}">
                    Visión
                </div>
                <div class="text-we">
                    Para el año 2024 llegar a ser una institución de alta complejidad acreditada, líder en el
                    departamento, con la mejor tecnología, autosostenible, con unos servicios acordes con
                    las necesidades de la población, con instalaciones amplias y modernas, contara con
                    recurso humano suficiente, comprometido, competitivo y con la mas alta calidad
                    académica técnica y científica, bien remunerado y con alto nivel de bienestar. Brindando
                    servicios de salud seguros con calidad y calidez, centrados en el usuario y su familia.
                </div>
            </div>
            <div class="col-md-6">
                <img src="{{ asset('img/vision.jpg') }}" class="w-100">
            </div>
        </div>
        <!-- <hr>
        <div class="row row-we">
            <div class="col-md-12">
                <div class="title-we">
                    <img src="{{ asset('img/Logo_curvas.png') }}">
                    Política Institucional de Seguridad del Paciente
                </div>
                <div class="text-we">
                    En la Hospital regional de Moniquirá nos comprometemos a prestar servicios de salud seguros, a
                    través de la prevención y gestión del riesgo en la seguridad del paciente, mediante acciones
                    asistenciales, administrativas y financieras eficientes, con talento humano competente, comprometido
                    y humanizado; utilizando recursos tecnológicos, infraestructura y equipamiento adecuado para
                    controlar infecciones, optimizar el uso de antibióticos. A partir del aprendizaje continuo fomentar
                    una cultura justa, no punitiva que genere confianza en el paciente y la familia.<br><br>
                    Dentro de la política de seguridad del paciente se establecen las siguientes excepciones para el
                    acto punitivo:<br>
                    Faltas graves contra los usuarios que vulneren cualquiera de sus derechos.
                    La ejecución de actos inseguros y repetitivos.<br>
                    Incumplimiento de las acciones de mejoramiento planteadas desde el comité de seguridad del paciente.
                </div>
            </div>
        </div> -->
    </div>
@endsection
