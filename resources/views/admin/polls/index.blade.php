@extends('layouts.app')

@section('site')
    <div class="d-none">
        {!! $site = 'polls' !!}
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Encuestas</div>
            <div class="card-body">
                <a href="{{ route('polls.create') }}" class="btn btn-success"><i class="fas fa-plus-circle"></i> Crear encuesta</a>
                <hr>
                @include('flash::message')
                @if($polls->count() > 0)
                    <table class="table">
                        <thead>
                            <th scope="col" style="width: 10%;">Nombre</th>
                            <th scope="col" style="width: 65%;">Link</th>
                            <th scope="col" style="width: 25%;">Accion</th>
                        </thead>
                        <tbody>
                            @foreach($polls as $poll)
                                <tr>
                                    <td>{{ $poll->name }}</td>
                                    <td>{{ $poll->link }}</td>
                                    <td>
                                        <a href="{{ route('polls.destroy', $poll->id) }}" class="btn btn-danger" title="Eliminar"><i class="far fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $polls->render() }}
                @else
                    <div class="alert alert-warning" role="alert">
                        No existen registros en base de datos. Para iniciar a crearlos, haz clic en <a href="{{ route('polls.create') }}" class="alert-link">Crear encuesta</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
