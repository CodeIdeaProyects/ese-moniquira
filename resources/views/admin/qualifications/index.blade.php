@extends('layouts.app')

@section('site')
    <div class="d-none">
        {!! $site = 'votes' !!}
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Calificaciones</div>
            <div class="card-body">       
                <hr>
                @include('flash::message')
                @if($qualifications->count() > 0)
                    <table class="table">
                        <thead>
                            <th scope="col" style="width: 20%;">Nombre calificador</th>
                            <th scope="col" style="width: 20%;">Nombre calificado</th>
                            <th scope="col" style="width: 15%;">Valor calificado</th>
                            <th scope="col" style="width: 15%;">Principio calificado</th>
                            <th scope="col" style="width: 30%;">Justificacion</th>
                        </thead>
                        <tbody>
                            @foreach($qualifications as $qualification)

                                    <td>{{ $qualification->user->name }}</td>
                                    <td>{{ $qualification->userQ->name }}</td>
                                    <td>{{ $qualification->aspect->name }}</td>
                                    <td>{{ $qualification->begining->name }}</td>
                                    <td>{!! $qualification->justify !!}</td>
                                    
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $qualifications->render() }}
                    <table class="table">
                        <thead>
                            <th scope="col" style="width: 20%;">Nombre calificado</th>
                            <th scope="col" style="width: 10%;">Votos</th>
                        </thead>
                        <tbody>
                            @foreach($rankings as $ranking)
                                    <td>{{ $ranking->name}}</td>
                                    <td>{{ $ranking->user_count}}</td>                              
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $rankings->render() }}
                @else
                    <div class="alert alert-warning" role="alert">
                        No existen calificaciones para esta votacion
                    </div>
                @endif
            </div>  
        </div>
    </div>
@endsection
