@extends('layouts.app')

@section('site')
    <div class="d-none">
        {!! $site = 'categories' !!}
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Crear proceso</div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if($errors->count() > 0)
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['route' => 'categories.store', 'method' => 'post', 'files'=>true]) !!}
                    @csrf
                    <div class="form-group row">
                        {!! Form::label('nombre', 'Nombre', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('nombre', old('nombre'), ['class' => 'form-control', 'placeholder' => 'Digite el nombre del proceso.']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('tipo_proceso', 'Tipo de proceso', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                           {!! Form::select('tipo_proceso', ['Padre' => 'Macroproceso', 'Hija' => 'Subproceso'], null, ['class' => 'form-control','placeholder' => 'Seleccione...', 'id' => 'tipo_proceso']) !!}
                       </div>
                    </div>
                    <div class="form-group row d-none" id="macroproceso">
                        {!! Form::label('macroproceso', 'Macroproceso', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                           {!! Form::select('macroproceso', $categories, null, ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                       </div>
                    </div>
                     <div class="form-group row d-none" id="codificacion">
                        {!! Form::label('codificacion', 'Codificación', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                          {!! Form::text('codificacion', old('codificacion'), ['class' => 'form-control', 'placeholder' => 'Digite la codificacion a usar en el código del documento.']) !!}
                       </div>
                    </div>

                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('categories.index') }}" class="btn btn-danger">Volver</a>
               {!! Form::close() !!}
           </div>
       </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            
            if($('#tipo_proceso').val() === 'Hija'){
                $('#macroproceso').removeClass('d-none');
                $('#codificacion').removeClass('d-none');
            }else{
                $('#macroproceso').addClass('d-none');
                $('#codificacion').addClass('d-none');
            }
       
            $('#tipo_proceso').change(function(e){
                if($(this).val() === 'Hija'){
                    $('#macroproceso').removeClass('d-none');
                    $('#codificacion').removeClass('d-none');
                }else{
                    $('#macroproceso').addClass('d-none');
                    $('#codificacion').addClass('d-none');
                }
            });
        });
    </script>
@endsection