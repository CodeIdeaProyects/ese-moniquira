@extends('layouts.app')

@section('site')
    <div class="d-none">
        {!! $site = 'categories' !!}
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Procesos</div>
            <div class="card-body">
                <a href="{{ route('categories.create') }}" class="btn btn-success"><i class="fas fa-plus-circle"></i> Crear proceso</a>
                 {!! Form::open(['route' => 'categories.index', 'method' => 'GET', 'class' => 'form-inline form-buscar']) !!}
                    <div class="input-group mb-3">
                        {!! Form::text('name', $search, ['class' => 'form-control', 'placeholder' => 'Buscar proceso...', 'aria-describedby' => 'search']) !!}
                        <div class="input-group-append">
                            <button class="btn btn-success" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                        @if(!is_null($search))
                            <a href="{{ route('categories.index') }}" class="btn btn-danger">
                                Limpiar <i class="fas fa-eraser"></i>
                            </a>
                        @endif
                    </div>
                {!! Form::close() !!}
                <hr>
               
                @include('flash::message')
                @if($categories->count() > 0)
                    <table class="table">
                        <thead>
                            <th scope="col" style="width: 10%;">ID</th>
                            <th scope="col" style="width: 30%;">Nombre</th>
                            <th scope="col" style="width: 30%;">Tipo proceso</th>
                            <th scope="col" style="width: 30%;">Accion</th>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->id }}</td>
                                    <td>{{ $category->name }}</td>
                                    @if(is_null($category->category_id))
                                        <td>Macroproceso</td>
                                    @else
                                        <td>Subproceso de <strong>{{ $category->category->name }}</strong></td>
                                    @endif
                                    <td>
                                        <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-info" title="Editar"><i class="fas fa-edit"></i></a>
                                        @if(($category->categories->count() > 0) || ($category->forms->count() > 0))
                                            <a href="{{ route('categories.destroy', $category->id) }}" class="btn btn-danger disabled" title="Información asociada"><i class="far fa-trash-alt"></i></a>
                                        @else
                                            <a href="{{ route('categories.destroy', $category->id) }}" class="btn btn-danger" title="Eliminar"><i class="far fa-trash-alt"></i></a>
                                        @endif
                                        <a href="{{ route('categories.show', $category->slug) }}" class="btn btn-warning" title="Ver" target="_blank"><i class="fas fa-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $categories->setPath('')->appends(Request::except('page'))->render() }}
                @else
                    <div class="alert alert-warning" role="alert">
                        No existen registros en base de datos. Para iniciar a crearlos, haz clic en <a href="{{ route('categories.create') }}" class="alert-link">Crear proceso</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
