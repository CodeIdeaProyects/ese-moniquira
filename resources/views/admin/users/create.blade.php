@extends('layouts.app')

@section('site')
    <div class="d-none">
        {!! $site = 'users' !!}
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Crear usuario</div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if($errors->count() > 0)
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['route' => 'users.store', 'method' => 'post']) !!}
                    @csrf
                    <div class="form-group row">
                        {!! Form::label('nombre', 'Nombre', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('nombre', old('nombre'), ['class' => 'form-control', 'placeholder' => 'Digite el nombre del usuario.']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('apellido', 'Apellido', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('apellido', old('apellido'), ['class' => 'form-control', 'placeholder' => 'Digite el apellido del usuario.']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('email', 'Email', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Digite el correo electrónico del usuario.']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('tipo_identificacion', 'Tipo de identificación', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::select('tipo_identificacion', ['CC' => 'Cédula de ciudadanía', 'CE' => 'Cédula de extranjería', 'TI' => 'Tarjeta de identidad', 'PA' => 'Pasaporte'], old('tipo_identificacion'), ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('funciones', 'Funciones del usuario', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::select('funciones', ['Administrativas' => 'Administrativas', 'Asistenciales' => 'Asistenciales', 'Servicios de apoyo' => 'Servicios de apoyo'], old('funciones'), ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('numero_identificacion', 'Numero de identificación', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('numero_identificacion', old('numero_identificacion'), ['class' => 'form-control', 'placeholder' => 'Digite el numero de identificación del usuario.']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('genero', 'Genero', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="radio col-sm-10">
                            <input type="radio" name="genero" value="M" @if(old('genero') === 'M') checked @endif class="form-control" id="genero_male">
                            {!! Form::label('genero_male', 'Masculino') !!}
                            <input type="radio" name="genero" value="F" @if(old('genero') === 'F') checked @endif class="form-control" id="genero_female">
                            {!! Form::label('genero_female', 'Femenino') !!}
                            <input type="radio" name="genero" value="O" @if(old('genero') === 'O') checked @endif class="form-control" id="genero_otro">
                            {!! Form::label('genero_otro', 'Otro') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('rol', 'Rol', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::select('rol', ['root' => 'Superadmin', 'admin' => 'Admin', 'user' => 'Usuario'], old('rol'), ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('fecha_nacimiento', 'Fecha de nacimiento', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::date('fecha_nacimiento', old('fecha_nacimiento'), ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('contrasena', 'Contraseña', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::password('contrasena', ['class' => 'form-control', 'placeholder' => 'Digite la contraseña para acceder al sistema']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('contrasena_confirmation', 'Confirme la contraseña', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::password('contrasena_confirmation', ['class' => 'form-control', 'placeholder' => 'Digite nuevamente la contraseña del usuario.']) !!}
                        </div>
                    </div>
                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('users.index') }}" class="btn btn-danger">Volver</a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
