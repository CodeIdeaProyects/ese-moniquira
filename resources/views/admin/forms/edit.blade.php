@extends('layouts.app')

@section('site')
<div class="d-none">
    {!! $site = 'forms' !!}
</div>
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">Editar documento <strong>{{ $form->name }}</strong></div>
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if($errors->count() > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['route' => ['forms.update',$form->id], 'method' => 'put', 'files' => true]) !!}
                @csrf
                <div class="form-group row">
                    {!! Form::label('nombre', 'Nombre', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('nombre', old('nombre', $form->name), ['class' => 'form-control', 'placeholder' => 'Digite el nombre del formulario.']) !!}
                    </div>
                </div>
                <div class="form-group row d-none">
                        {!! Form::label('tipo_codigo', 'Digitacion de codigo', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                         {!! Form::select('tipo_codigo', ['Automatico' => 'Automatico', 'Manual' => 'Manual'], old('tipo_codigo',$form->type_cod), ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('subproceso', 'Subproceso', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                       {!! Form::select('subproceso', $categories, old('subproceso', $form->category_id), ['class' => 'form-control', 'placeholder' => 'Seleccione...', 'disabled']) !!}
                   </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('tipo_formulario', 'Tipo de formulario', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::select('tipo_formulario', $types, old('tipo_formulario', $form->type_id), ['class' => 'form-control', 'placeholder' => 'Seleccione...','disabled']) !!}
                    </div>
                </div>
                <div class="form-group row">
                        {!! Form::label('codificacion', 'Codificación', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                         {!! Form::text('codificacion', old('codificacion', $form->type_cod), ['class' => 'form-control disabled', 'placeholder' => 'Seleccione...', 'readonly']) !!}
                    </div>
                </div>
                @if($form->type_cod == 'Manual')
                    <div class="form-group row">
                        {!! Form::label('codigo', 'Código', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('codigo', old('codigo', $form->code), ['class' => 'form-control', 'placeholder' => 'Digite el nombre del codigo.']) !!}
                        </div>
                    </div>
                @endif
                <div class="form-group row">
                    {!! Form::label('observacion', 'Observación', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::textarea('observacion', old('observacion'), ['class' => 'form-control', 'placeholder' => 'Digite la justiciación de la modificación del documento.', 'maxlength'=>'280', 'rows'=>'4']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('evidencia_label', 'Evidencia', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        <label for="evidencia" class="label-evidencia"><i class="fas fa-file-upload"></i> Selecciona la evidencia</label>
                        {!! Form::file('evidencia', ['class' => 'd-none', 'id' => 'evidencia', 'accept' => '.doc,.docx,.xml,.pdf,.xlsx,.zip,.rar,.xls,.pptx']) !!}
                        <small id="evidenceHelp" class="form-text text-muted">Formatos aceptados: .doc .docx .xml .pdf .xlsx .zip .rar .pptx (Evite usar espacios en el nombre del archivo)</small>
                    </div>
                </div>
                <div class="form-group row" id="container-preview-doc">
                    {!! Form::label('preview', 'Nombre documento', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        <div class="alert alert-secondary" role="alert" id="document-preview">
                            Documento actual: <a href="{{ asset('storage/forms') }}/{{ $form->document }}" class="alert-link">{{ $form->document }}</a>
                        </div>
                    </div>
                </div>
                {!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('forms.index') }}" class="btn btn-danger">Volver</a>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
   
        $('#evidencia').change(function(e) {
            $('#container-preview-doc').removeClass('d-none');
            var info = $('#evidencia')[0].files[0].name;
            $('#document-preview').html(info + ": Listo para subir!");
        });
    </script>
@endsection