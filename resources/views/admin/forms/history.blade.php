@extends('layouts.app')

@section('site')
<div class="d-none">
    {!! $site = 'forms' !!}
</div>
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">Historial para <strong>{{ $form->name }}</strong></div>
        <div class="card-body">
            <hr>
            @include('flash::message')
            <table class="table">
                <thead>
                    <th scope="col" style="width: 15%;">Nombre formulario</th>
                    <th scope="col" style="width: 15%;">Categoria</th>
                    <th scope="col" style="width: 15%;">Modificó usuario</th>
                    <th scope="col" style="width: 15%;">Tipo</th>
                    <th scope="col" style="width: 10%;">Versión</th>
                    <th scope="col" style="width: 5%;">Codigo</th>
                    <th scope="col" style="width: 10%;">Modificación</th>
                    <th scope="col" style="width: 15%;">Observacion</th>
                </thead>
                <tbody>
                    @foreach($forms as $form)
                        <tr>
                            <td>{{ $form->name }}</td>
                            <td>{{ $form->category->name }}</td>
                            <td>{{ $form->user->name }}</td>
                            <td>{{ $form->type->name }}</td>
                            <td>{{ $form->version }}</td>
                            <td>{{ $form->code }}</td>
                            <td>{{ $form->created_at}}</td>
                            <td>
                                @if(is_null($form->observation))
                                    - Creación -
                                @else
                                    {{ $form->observation }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <a href="{{ route('forms.index') }}" class="btn btn-danger">Volver</a>
            <a href="{{ route('forms.reports', $form->id) }}" class="btn btn-success" title="Exportar historial">Exportar historial <i class="fas fa-file-excel"></i></a>
        </div>
    </div>
</div>
@endsection