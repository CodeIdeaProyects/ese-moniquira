@extends('layouts.app')

@section('site')
<div class="d-none">
    {!! $site = 'forms' !!}
</div>
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">Documentos</div>
        <div class="card-body">
            <a href="{{ route('forms.create') }}" class="btn btn-success"><i class="fas fa-plus-circle"></i> Crear documento</a>
            <a href="{{ route('forms.reportsDocuments') }}" class="btn btn-success"><i class="fas fa-file-excel"></i> Exportar información</a>
             {!! Form::open(['route' => 'forms.index', 'method' => 'GET', 'class' => 'form-inline form-buscar']) !!}
                <div class="input-group mb-3">
                    {!! Form::text('name', $search, ['class' => 'form-control', 'placeholder' => 'Buscar formulario...', 'aria-describedby' => 'search']) !!}
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                        @if(!is_null($search))
                            <a href="{{ route('forms.index') }}" class="btn btn-danger">
                                Limpiar <i class="fas fa-eraser"></i>
                            </a>
                        @endif
                    </div>
                </div>
            {!! Form::close() !!}
            <hr>
            @include('flash::message')
            @if($forms->count() > 0)
                <table class="table">
                    <thead>
                        <th scope="col" style="width: 15%;">Nombre</th>
                        <th scope="col" style="width: 15%;">Subproceso</th>
                        <th scope="col" style="width: 15%;">Usuario</th>
                        <th scope="col" style="width: 15%;">Tipo</th>
                        <th scope="col" style="width: 10%;">Versión</th>
                        <th scope="col" style="width: 10%;">Codigo</th>
                        <th scope="col" style="width: 20%;">Acciones</th>
                    </thead>
                    <tbody>
                        @foreach($forms as $form)
                            <tr>
                                <td>{{ $form->name }}</td>
                                <td>{{ $form->category->name }}</td>
                                <td>{{ $form->user->name }}</td>
                                <td>{{ $form->type->name }}</td>
                                <td>{{ $form->version }}</td>
                                <td>{{ $form->code }}</td>
                                <td>
                                    <a href="{{ route('forms.edit', $form->id) }}" class="btn btn-info"><i class="fas fa-edit" title="Editar"></i></a>
                                    <a href="{{ route('forms.destroy', $form->id) }}" class="btn btn-danger" title="Eliminar"><i class="far fa-trash-alt"></i></a>
                                    <a href="{{ asset('storage/forms') }}/{{ $form->document }}" target="_blank" class="btn btn-success" title="Descargar"><i class="fas fa-download"></i></a>
                                    <a href="{{ route('forms.history', $form->id) }}" class="btn btn-info" title="Historial"><i class="fas fa-history"></i></a>
                               </td>
                           </tr>
                       @endforeach
                   </tbody>
               </table>
               {{ $forms->setPath('')->appends(Request::except('page'))->render() }}
            @else
               <div class="alert alert-warning" role="alert">
                No existen registros en base de datos. Para iniciar a crearlos, haz clic en <a href="{{ route('forms.create') }}" class="alert-link">Crear documento</a>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection