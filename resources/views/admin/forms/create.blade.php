@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/trumbowyg/dist/ui/trumbowyg.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/trumbowyg/dist/plugins/table/ui/trumbowyg.table.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/chosen/chosen.min.css') }}">
@endsection

@section('site')
    <div class="d-none">
        {!! $site = 'forms' !!}
    </div>
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">Crear documento</div>
        <div class="card-body">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
            @if($errors->count() > 0)
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            {!! Form::open(['route' => 'forms.store', 'method' => 'post','files' => true]) !!}
                @csrf
                <div class="form-group row">
                    {!! Form::label('nombre', 'Nombre', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('nombre', old('nombre'), ['class' => 'form-control', 'placeholder' => 'Digite el nombre del documento.']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('macroproceso', 'Macroproceso', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::select('macroproceso', $categories, old('macroproceso'), ['class' => 'form-control', 'id' => 'macroproceso', 'placeholder' => 'Seleccione...']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('subproceso', 'Subproceso', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::select('subproceso', ['Seleccione...' => 'Seleccione...'], old('subproceso'), ['class' => 'form-control', 'id' => 'subproceso', 'placeholder' => 'Seleccione...', 'disabled']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('tipo_formulario', 'Tipo de formulario', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::select('tipo_formulario', $types, old('tipo_formulario'), ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                    </div>
                </div>
                <div class="form-group row">
                        {!! Form::label('codificacion', 'Codificación', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                         {!! Form::select('codificacion', ['Automatico' => 'Automatico', 'Manual' => 'Manual'], old('codificacion'), ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                    </div>
                </div>
                <div class="form-group row d-none" id="nombre_codigo">
                    {!! Form::label('codigo', 'Código', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('codigo', old('codigo'), ['class' => 'form-control', 'placeholder' => 'Digite el nombre del codigo.']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('evidencia_label', 'Evidencia', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        <label for="evidencia" class="label-evidencia"><i class="fas fa-file-upload"></i> Selecciona la evidencia</label>
                        {!! Form::file('evidencia', ['class' => 'd-none', 'id' => 'evidencia', 'accept' => '.doc,.docx,.xml,.pdf,.xlsx,.zip,.rar,.xls,.pptx']) !!}
                        <small id="evidenceHelp" class="form-text text-muted">Formatos aceptados: .doc .docx .xml .pdf .xlsx .zip .rar .pptx (Evite usar espacios en el nombre del archivo)</small>
                    </div>
                </div>
                <div class="form-group row d-none" id="container-preview-doc">
                    {!! Form::label('preview', 'Nombre documento', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        <div class="alert alert-secondary" role="alert" id="document-preview">

                        </div> 
                    </div>
                </div>
                
                {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('forms.index') }}" class="btn btn-danger">Volver</a>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('scripts')
{!! Html::script('plugins/trumbowyg/dist/trumbowyg.min.js') !!}
{!! Html::script('plugins/trumbowyg/dist/langs/es.min.js') !!}
{!! Html::script('plugins/chosen/chosen.jquery.js') !!}
    <script type="text/javascript">
       $('#codificacion').change(function(e){
            if($(this).val() === 'Manual'){
                $('#nombre_codigo').removeClass('d-none');
            }else{
                $('#nombre_codigo').addClass('d-none');
            }
        });

        $('#evidencia').change(function(e) {
           $('#container-preview-doc').removeClass('d-none');
           var info = $('#evidencia')[0].files[0].name;
           $('#document-preview').html(info + ": Listo para subir!");
        });
        $('#macroproceso').chosen({
                no_results_text: "No hay coincidencias"
            });
        $('#tipo_formulario').chosen({
                no_results_text: "No hay coincidencias"
            });


        $("#macroproceso").change(function(event){
        
            $("#subproceso").removeAttr("disabled");
            
            if(event.target.value !== ""){
                $.get("/admin/getProcess/"+event.target.value+"",function(response,state){
                    if(response.length !== 0){

                        $("#subproceso").html("");
                        $("#subproceso").append($("<option></option>").text("Seleccione..."));
                        for(var i = 0; i < response.length; i++){
                            $("#subproceso").append($("<option></option>").attr("value", response[i].id).text(response[i].name));
                        }
                    }else{
                        $("#subproceso").prop("disabled", "disabled");
                        $("#subproceso").html("");
                        $("#subproceso").append($("<option></option>").text("No existen registros"));
                    }
                });
            }else{
                $("#subproceso").prop("disabled", "disabled");
            }
        });
        
    </script>
@endsection