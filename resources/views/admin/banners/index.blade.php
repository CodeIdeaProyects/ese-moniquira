@extends('layouts.app')

@section('site')
    <div class="d-none">
        {!! $site = 'banners' !!}
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Banners</div>
            <div class="card-body">
                <a href="{{ route('banners.create') }}" class="btn btn-success"><i class="fas fa-plus-circle"></i> Crear banner</a>
                <hr>
                @include('flash::message')
                @if($banners->count() > 0)
                    <table class="table">
                        <thead>
                            <th scope="col" style="width: 10%;">Id</th>
                            <th scope="col" style="width: 65%;">Imagen</th>
                            <th scope="col" style="width: 25%;">Accion</th>
                        </thead>
                        <tbody>
                            @foreach($banners as $banner)
                                <tr>
                                    <td>{{ $banner->id }}</td>
                                    <td>
                                        <img class="img-preview-banner" src="{{ asset('storage/banners') }}/{{ $banner->image }}">
                                    </td>
                                    <td>
                                        <a href="{{ route('banners.edit', $banner->id) }}" class="btn btn-info" title="Editar"><i class="fas fa-edit"></i></a>
                                        <a href="{{ route('banners.destroy', $banner->id) }}" class="btn btn-danger" title="Eliminar" onclick="return confirm('Está seguro de que desea eliminar el registro?')"><i class="far fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $banners->render() }}
                @else
                    <div class="alert alert-warning" role="alert">
                        No existen registros en base de datos. Para iniciar a crearlos, haz clic en <a href="{{ route('banners.create') }}" class="alert-link">Crear banner</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
