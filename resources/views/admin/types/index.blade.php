@extends('layouts.app')

@section('site')
    <div class="d-none">
        {!! $site = 'types' !!}
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Tipos de formularios</div>
            <div class="card-body">
                <a href="{{ route('types.create') }}" class="btn btn-success"><i class="fas fa-plus-circle"></i> Crear tipo de formulario</a>        
                {!! Form::open(['route' => 'types.index', 'method' => 'GET', 'class' => 'form-inline form-buscar']) !!}
                    <div class="input-group mb-3">
                        {!! Form::text('name', $search, ['class' => 'form-control', 'placeholder' => 'Buscar tipo...', 'aria-describedby' => 'search']) !!}
                        <div class="input-group-append">
                            <button class="btn btn-success" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                            @if(!is_null($search))
                                <a href="{{ route('types.index') }}" class="btn btn-danger">
                                    Limpiar <i class="fas fa-eraser"></i>
                                </a>
                            @endif
                        </div>
                    </div>
                 {!! Form::close() !!}
                <hr>
                @include('flash::message')
                @if($types->count() > 0)
                    <table class="table">
                        <thead>
                            <th scope="col" style="width: 35%;">Nombre</th>
                            <th scope="col" style="width: 35%;">Formularios asociados</th>
                            <th scope="col" style="width: 30%;">Acciones</th>
                        </thead>
                        <tbody>
                            @foreach($types as $type)
                                <tr>
                                    <td>{{ $type->name }}</td>
                                    <td>
                                        @if($type->forms->count() < 2)
                                            <span class="badge badge-danger">{{ $type->forms->count() }}</span>
                                        @elseif($type->forms->count() >= 2 && $type->forms->count() < 4)
                                            <span class="badge badge-primary">{{ $type->forms->count() }}</span>
                                        @else
                                            <span class="badge badge-success">{{ $type->forms->count() }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('types.edit', $type->id) }}" class="btn btn-info"><i class="fas fa-edit" title="Editar"></i></a>
                                        @if($type->forms->count() > 0)
                                            <a href="#" class="btn btn-danger disabled" title="Formularios asociados"><i class="far fa-trash-alt"></i></a>
                                        @else
                                            <a href="{{ route('types.destroy', $type->id) }}" class="btn btn-danger" title="Eliminar"><i class="far fa-trash-alt"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $types->setPath('')->appends(Request::except('page'))->render() }}
                @else
                    <div class="alert alert-warning" role="alert">
                        No existen registros en base de datos. Para iniciar a crearlos, haz clic en <a href="{{ route('types.create') }}" class="alert-link">Crear tipo de formulario</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
