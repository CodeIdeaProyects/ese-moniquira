@extends('layouts.main')

@section('menu')
    <div class="d-none">
        {!! $site = 'ext' !!}
    </div>
@endsection

@section('content')
    <div class="main-container">
        <div class="d-none">
            {!! $image = rand(0, 2); !!}
        </div>
        <img src="{{ asset($medico[$image]) }}" id="medico">
        <div class="container-bread">
            <a href="{{ route('home') }}" class="link-bread">Macroprocesos</a> / <a href="{{ route('categories.show', $macro->slug) }}" class="link-bread">{{ $macro->name }}</a> / <strong>{{ $sub->name }}</strong>
            {!! Form::open(['route' => 'forms.scopeF', 'method' => 'GET', 'class' => 'form-inline form-buscar']) !!}
                <div class="input-group mb-3">
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Buscar formulario...', 'aria-describedby' => 'search']) !!}
                    <div class="input-group-append">
                        <button class="btn btn-search" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
        <hr class="hr-intranet">
        <div class="row row-process">
            <div class="col-md-8">
                <div class="row">
                    @if($sub->forms->count() > 0)
                        @foreach($types as $type)
                            @if($type->forms->where('category_id', $sub->id)->where('state', 'Active')->count() > 0)
                                <table class="table">
                                    <thead>
                                        <th style="width: 30%; border-bottom: 2px dashed #529849; background-color: #FFFFFF; color: #529849;"><strong>Código</strong><img src="{{ asset('img/logo_ico.png') }}" class="img-table"></th>
                                        <th style="width: 70%; border-bottom: 2px dashed #529849; background-color: #FFFFFF; color: #529849;"><strong>{{ $type->name }}</strong><img src="{{ asset('img/logo_ico.png') }}" class="img-table"></th>
                                    </thead>
                                    <tbody>
                                        @foreach($type->forms->where('category_id', $sub->id)->where('state', 'Active') as $form)
                                            <tr>
                                                <td>{{ $form->code }}</td>
                                                @if($form->getFormat() === 'pdf')
                                                    <td><a href="{{ route('categories.show.pdf', $form->id) }}" class="link-document">{{ $form->name }}</a></td>
                                                @else
                                                    <td><a href="{{ asset('storage/forms') }}/{{ $form->document }}" class="link-document" target="_blank">{{ $form->name }}</a></td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        @endforeach
                    @else
                        <div class="alert-empty alert alert-warning" role="alert">
                            <img src="{{ asset('img/fly.png') }}">
                            <strong>Ups!</strong> No existen registros en este subproceso
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <ul class="list-categories">
                    @foreach($categories as $category_f)
                        <li class="@if($category_f->id === $macro->id) active @endif">
                            <a href="{{ route('categories.show', $category_f->slug) }}" class="link-menu">{{ $category_f->name }}</a> @if($category_f->categories->count() > 0) <button class="btn btn-menu" id="btn{{ $category_f->id }}" onclick="viewMenu({{ $category_f->id }})">@if($category_f->id === $macro->id) <i class="fas fa-angle-up"></i> @else <i class="fas fa-angle-down"></i> @endif</button> @endif
                            <ul class="list-subcategories @if($category_f->id === $macro->id) view @endif" id="list{{ $category_f->id }}">
                                @foreach($category_f->categories as $subcategory)
                                    <li class="@if($sub->id === $subcategory->id) active @endif"><a href="{{ route('categories.show.subprocess', [$category_f->slug, $subcategory->slug]) }}">{{ $subcategory->name }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        function viewMenu(id){
            $('#list' + id).toggleClass('view');
            if($('#btn' + id + ' i').hasClass('fa-angle-down')){
                $('#btn' + id + ' i').removeClass('fa-angle-down');
                $('#btn' + id + ' i').addClass('fa-angle-up');
            }else{
                $('#btn' + id + ' i').removeClass('fa-angle-up');
                $('#btn' + id + ' i').addClass('fa-angle-down');
            }
        }
    </script>
@endsection