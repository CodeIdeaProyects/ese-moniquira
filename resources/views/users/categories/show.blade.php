@extends('layouts.main')

@section('menu')
    <div class="d-none">
        {!! $site = 'ext' !!}
    </div>
@endsection

@section('content')
    <div class="main-container">
        <div class="d-none">
            {!! $image = rand(0, 2); !!}
        </div>
        <img src="{{ asset($medico[$image]) }}" id="medico">
        <div class="container-bread">
            <a href="{{ route('home') }}" class="link-bread">Macroprocesos</a> / <strong>{{ $category->name }}</strong>
            {!! Form::open(['route' => 'forms.scopeF', 'method' => 'GET', 'class' => 'form-inline form-buscar']) !!}
                <div class="input-group mb-3">
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Buscar formulario...', 'aria-describedby' => 'search']) !!}
                    <div class="input-group-append">
                        <button class="btn btn-search" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
        <hr class="hr-intranet">
        <div class="row row-process">
            <div class="col-md-8">
                <div class="row">
                    @if($category->categories->count() > 0)
                        @foreach($category->categories as $category_s)
                            <div class="d-none">
                                {!! $rand = rand(0, 5); !!}
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('categories.show.subprocess', [$category->slug, $category_s->slug]) }}" class="link-subprocess">
                                    <div class="img-subprocess" style="background-color: {{ $colors[$rand]  }};">
                                        <img src="{{ asset('img/subproceso.png') }}">
                                        <div>
                                            {{ $category_s->name }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <div class="alert-empty alert alert-warning" role="alert">
                            <img src="{{ asset('img/fly.png') }}">
                            <strong>Ups!</strong> No existen registros en este macroproceso
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <ul class="list-categories">
                    @foreach($categories as $category_f)
                        <li class="@if($category_f->id === $category->id) active @endif">
                            <a href="{{ route('categories.show', $category_f->slug) }}" class="link-menu">{{ $category_f->name }}</a> @if($category_f->categories->count() > 0) <button class="btn btn-menu" id="btn{{ $category_f->id }}" onclick="viewMenu({{ $category_f->id }})">@if($category_f->id === $category->id) <i class="fas fa-angle-up"></i> @else <i class="fas fa-angle-down"></i> @endif</button> @endif
                            <ul class="list-subcategories @if($category_f->id === $category->id) view @endif" id="list{{ $category_f->id }}">
                                @foreach($category_f->categories as $subcategory)
                                    <li><a href="{{ route('categories.show.subprocess', [$category_f->slug, $subcategory->slug]) }}">{{ $subcategory->name }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        function viewMenu(id){
            $('#list' + id).toggleClass('view');
            if($('#btn' + id + ' i').hasClass('fa-angle-down')){
                $('#btn' + id + ' i').removeClass('fa-angle-down');
                $('#btn' + id + ' i').addClass('fa-angle-up');
            }else{
                $('#btn' + id + ' i').removeClass('fa-angle-up');
                $('#btn' + id + ' i').addClass('fa-angle-down');
            }

        }
    </script>
@endsection