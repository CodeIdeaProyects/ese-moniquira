@extends('layouts.main')

@section('menu')
    <div class="d-none">
        {!! $site = 'ext' !!}
    </div>
@endsection

@section('content')
    <div class="main-container">
        <div>
            {{ $form->name }}
        </div>
        <hr>
        <div class="container-frame">
            <iframe src="{{ asset('storage/forms') }}/{{ $form->document }}#toolbar=0" id="pdfview"></iframe>
            <div class="tapar"></div>
            <div class="tapar-2" id="tapar-2"></div>
            <div id="alerta">
                <div class="msg-alerta">
                    No es posible descargar el contenido, puedes navegar en el documento usando la barra de scroll derecho
                    <br>
                    <button id="close" class="msg-close">Cerrar</button>
                </div>
            </div>
        </div> 
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function() {
            $(document).bind("contextmenu",function(e){
                return false;
            });
            $('#tapar-2').bind("contextmenu",function(e){
                $('#alerta').fadeIn('slow');
            });

            $('#close').click(function(e){
                $('#alerta').fadeOut('slow');
            });
        });
    </script>
@endsection