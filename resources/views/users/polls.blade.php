@extends('layouts.main')

@section('menu')
    <div class="d-none">
        {!! $site = 'ext' !!}
    </div>
@endsection

@section('content')
    <div class="main-container">
        <div class="msg-container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{ asset('img/medico.png') }}">
                    </div>
                    <div class="col-md-8">
                        <h1 class="title-polls">Encuestas</h1>
                        <hr>
                        @if($polls->count() > 0)
                            <table class="table table-polls">
                                <thead>
                                    <th style="width: 10%; border-bottom: 2px dashed #529849; background-color: #FFFFFF; color: #529849;"><strong>Id</strong></th>
                                    <th style="width: 90%; border-bottom: 2px dashed #529849; background-color: #FFFFFF; color: #529849;"><strong>Nombre de la encuesta</strong></th>
                                </thead>
                                <tbody>
                                    @foreach($polls as $poll)
                                        <tr>
                                            <td>{{ $poll->id }}</td>
                                            <td><a href="{{ $poll->link }}" class="link-document" target="_blank">{{ $poll->name }}</a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert-empty alert alert-warning" role="alert">
                                <img src="{{ asset('img/fly.png') }}">
                                <strong>Ups!</strong> No existen registros en este momento
                            </div>
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection