@extends('layouts.main')

@section('menu')
    <div class="d-none">
        {!! $site = 'ext' !!}
    </div>
@endsection

@section('content')
	<div class="main-container">
		<div class="msg-container">
			<div class="row row-novedad">
				<div class="col-md-6">
					<img src="{{ asset('img/medico2.png') }}">
				</div>
				<div class="col-md-6">
					<div class="download">
						<h1>Descarga las alertas sanitarias farmacovigilancia</h1>
						<hr class="hr-novedad">
						<a href="{{ asset('resources/fennyl.pdf') }}" target="_blank" class="btn btn-download"><i class="fas fa-download"></i><br>FENNYN</a>
						<a href="{{ asset('resources/fentanil.pdf') }}" target="_blank" class="btn btn-download"><i class="fas fa-download"></i><br>FENTANIL</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection