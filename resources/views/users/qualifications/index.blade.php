@extends('layouts.main')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/trumbowyg/dist/ui/trumbowyg.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/trumbowyg/dist/plugins/table/ui/trumbowyg.table.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/chosen/chosen.min.css') }}">
@endsection

@section('menu')
    <div class="d-none">
        {!! $site = 'ext' !!}
    </div>
@endsection

@section('content')
	<div class="main-container">
		@if($errors->count() > 0)
		    <div class="alert alert-danger" role="alert">
		    	Tienes algunos errores en el formulario, por favor verifica antes de enviar.
		        <ul>
		            @foreach($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<div class="info-vote">
			<h2>{{ $vote->name }}</h2>
			<div>
                Te conviertes en Funcionario de Excelencia cuando tus compañeros te han enviado varias nominaciones reconociendo los valores, el trato humanizado en tus actitudes y/o las aptitudes con las cuales haces tus labores diarias. Si recolectas muchas nominaciones recibirás un reconocimiento especial en un evento importante para el hospital.<br>
				Recuerda que puedes votar desde <strong>{{ $vote->date_start }}</strong> hasta <strong>{{ $vote->date_end }}</strong>.<br>
				Cada usuario dispone de un máximo de <strong>{{ $vote->maximum }}</strong> votos.
			</div>
		</div>
		<hr>
		{!! Form::open(['route' => 'qualifications.store', 'method' => 'POST']) !!}
			@csrf
	        {!! Form::hidden('votacion', $vote->id, old('votaciones'), ['class' => 'form-control']) !!}
			
	        <div class="form-group row">
                {!! Form::label('Funciones', 'Funciones', ['class' => 'col-sm-2 col-form-label']) !!}
                <div class="col-sm-10">
                    {!! Form::select('funciones', ['Administrativas' => 'Administrativas', 'Asistenciales' => 'Asistenciales', 'servicios de apoyo' => 'Servicios de apoyo'], old('funciones'), ['class' => 'form-control', 
                    'id' => 'funciones',
                    'placeholder' => 'Seleccione...']) !!}
                </div>
            </div>

			<div class="form-group row">
                    {!! Form::label('usuario', 'Usuario', ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::select('usuario', ['Seleccione...' => 'Seleccione...'], old('usuario'), ['class' => 'form-control', 'id' => 'usuario', 'disabled']) !!}
                    </div>
                </div>
			<div class="form-group row">
			    {!! Form::label('aspecto', 'Valor', ['class' => 'col-sm-2 col-form-label']) !!}
			    <div class="col-sm-10">
			        {!! Form::select('aspecto', $aspects, old('aspecto'), ['class' => 'form-control', 'placeholder' => 'Selecciona...']) !!}
			    </div>
		    </div>
		    <div class="form-group row">
			    {!! Form::label('principio', 'Principio', ['class' => 'col-sm-2 col-form-label']) !!}
			    <div class="col-sm-10">
			        {!! Form::select('principio', $beginings, old('principio'), ['class' => 'form-control', 'placeholder' => 'Selecciona...']) !!}
			    </div>
		    </div>
		   
		    <div class="form-group row">
			    {!! Form::label('justificacion', 'Justificación', ['class' => 'col-sm-2 col-form-label']) !!}
			    <div class="col-sm-10">
			        {!! Form::textarea('justificacion', old('justificacion'), ['class' => 'form-control', 'placeholder' => 'Digita las razones que justifican tu elección']) !!}
			    </div>
		    </div>
			{!! Form::submit('Votar', ['class' => 'btn btn-clínica_del_trabajador btn-vote']) !!}
			<hr class="hr-fin">
		{!! Form::close() !!}
	</div>
@endsection

@section('scripts')
    {!! Html::script('plugins/trumbowyg/dist/trumbowyg.min.js') !!}
    {!! Html::script('plugins/trumbowyg/dist/langs/es.min.js') !!}
    {!! Html::script('plugins/chosen/chosen.jquery.js') !!}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#justificacion').trumbowyg({
                lang: 'es',
                btns: [
                    ['viewHTML'],
                    ['strong', 'em', 'del'],
                    ['superscript', 'subscript'],
                    ['link'],
                    ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                    ['unorderedList', 'orderedList'],
                    ['horizontalRule'],
                ]
            });

            $('#aspecto').chosen({
                no_results_text: "No hay coincidencias"
            });

            $('#principio').chosen({
                no_results_text: "No hay coincidencias"
            });

        });
        $("#funciones").change(function(event){
            $("#usuario").removeAttr("disabled");
            if(event.target.value !== ""){
                $.get("/user/getUsers/"+event.target.value+"",function(response,state){
                    if(response.length !== 0){
                        $("#usuario").chosen("destroy");
                        $("#usuario").html("");
                        $("#usuario").append($("<option></option>").text("Seleccione..."));
                        for(var i = 0; i < response.length; i++){
                            $("#usuario").append($("<option></option>").attr("value", response[i].id).text(response[i].name + " " + response[i].lastname));
                        }
                        $('#usuario').chosen({
                            no_results_text: "No hay coincidencias"
                        });
                    }else{
                        $("#usuario").chosen("destroy");
                        $("#usuario").prop("disabled", "disabled");
                        $("#usuario").html("");
                        $("#usuario").append($("<option></option>").text("No existen registros"));
                    }
                });
            }else{
                $("#usuario").chosen("destroy");
                $("#usuario").prop("disabled", "disabled");
            }
        });
    </script>
@endsection