@extends('layouts.main')

@section('menu')
    <div class="d-none">
        {!! $site = 'ext' !!}
    </div>
@endsection

@section('content')
	<div class="main-container">
		<div class="msg-container">
			<img src="{{ asset('img/medico.png') }}">
			<hr>
			@if($flag)
				<h1>Felicitaciones! Haz realizado el proceso de calificación exitosamente</h1>
			@else
				<h1>Sucedió un error al calificar al usuario, por favor intentalo nuevamente</h1>
			@endif
			<a href="{{ url('/') }}" class="btn btn-clínica_del_trabajador">Inicio</a>
		</div>
	</div>
@endsection