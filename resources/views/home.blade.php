@extends('layouts.main')

@section('menu')
    <div class="d-none">
        {!! $site = 'ext' !!}
    </div>
@endsection

@section('content')
    <div class="main-container">
        @if(Auth::user()->isRoot() || Auth::user()->isAdmin())
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="wellcome-text">
                        Bienvenido a la intranet de la Hospital regional de Moniquirá.<br>
                        Selecciona uno de los macroprocesos para ver la información.<br>
                        Si deseas acceder al panel administrativo, haz clic en el siguiente botón<br>
                        <a href="{{ route('admin') }}" class="btn btn-clínica_del_trabajador">Panel administrativo</a>
                    </div>
                </div>
            </div>
        @else
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="wellcome-text">
                        Bienvenido a la intranet de la Hospital regional de Moniquirá.<br>
                        Selecciona uno de los macroprocesos para ver la información.
                    </div>
                </div>
            </div>
        @endif
        <hr>
        <div class="text-center">
            <img src="{{ asset('img/logo-procesos.png') }}" style="width: 150px">
        </div>
        <div id="process-image">
            <img src="{{ asset('img/mapaprocesos.png') }}" class="w-100">
            <a href="{{ url('/') }}/user/macroprocesos/seguridad-del-paciente" class="process process1"></a>
            <a href="{{ url('/') }}/user/macroprocesos/direccionamiento-estrategico" class="process process2"></a>
            <a href="{{ url('/') }}/user/macroprocesos/gestion-de-calidad" class="process process3"></a>
            <a href="{{ url('/') }}/user/macroprocesos/unidades-funcionales" class="process process4"></a>
            <a href="{{ url('/') }}/user/macroprocesos/humanizacion" class="process process5"></a>
            <a href="{{ url('/') }}/user/macroprocesos/gestion-del-riesgo" class="process process6"></a>
            <a href="{{ url('/') }}/user/macroprocesos/gestion-de-tecnologia" class="process process7"></a>
            <a href="{{ url('/') }}/user/macroprocesos/gestion-integral-al-usuario" class="process process8"></a>
            <a href="{{ url('/') }}/user/macroprocesos/gestion-del-ciclo-economico" class="process process9"></a>
            <a href="{{ url('/') }}/user/macroprocesos/gestion-de-recursos-fisicos" class="process process10"></a>
            <a href="{{ url('/') }}/user/macroprocesos/gestion-financiera" class="process process11"></a>
            <a href="{{ url('/') }}/user/macroprocesos/gestion-administrativa"
               class="process process12"></a>
            <a href="{{ url('/') }}/user/macroprocesos/evaluacion-y-control" class="process process13"></a>
            <a href="{{ url('/') }}/user/macroprocesos/cultura-de-mejoramiento" class="process process14"></a>
            <a href="{{ url('/') }}/user/macroprocesos/responsabilidad-social" class="process process15"></a>
        </div>
    </div>
@endsection
