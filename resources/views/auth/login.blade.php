@extends('layouts.main')

@section('menu')
    <div class="d-none">
        {!! $site = 'ext' !!}
    </div>
@endsection

@section('content')
    <div id="login-container">
        <div class="main-container-auth">
            <div class="card-container">
                <div class="card" id="card-login">
                    <div class="card-header">{{ __('Login') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group row-login">
                                <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group row-login">
                                <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group row-btns-login">
                                <div class="checkbox">
                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} class="form-control">
                                    {!! Form::label('remember', 'Recuérdame') !!}
                                </div>
                            </div>
                            <div class="form-group row-btns-login" id="row-autorizacion">
                                <div class="checkbox">
                                    <input type="checkbox" name="politica" id="politica" {{ old('politica') ? 'checked' : '' }} class="form-control">
                                    {!! Form::label('politica', 'Autorizo el tratamiento de datos personales') !!}
                                </div>
                                 <small id="emailHelp" class="form-text text-muted"><a href="{{ asset('files/autorizacion.pdf') }}" target="_blank" class="link-muted">Autorización tratamiento de datos personales:</a> en cumplimiento a lo establecido en el Decreto 1377 de 2013 reglamentario de la Ley 1851 de 2012, la empresa INAFIC SAS solicita leer detalladamente el adjunto con el fin de que conozca las políticas institucionales establecidas para el tratamiento de sus datos personales.</small>
                            </div>
                            <div class="form-group row-btns-login mb-0">
                                <button type="submit" class="btn btn-clínica_del_trabajador">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn link-clínica_del_trabajador" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(e){
            if( !localStorage.getItem('ingreso') ){
                $('#row-autorizacion').css({'display':'block'});
                $('#politica').attr('required', 'required');
                localStorage.setItem('ingreso',1); 
            } else {
                $('#row-autorizacion').css({'display':'none'});
                $('#politica').removeAttr("required");
            }
        });
    </script>
@endsection