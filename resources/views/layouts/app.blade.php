<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Hospital regional de Moniquirá</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('img/logo_ico.png') }}"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    @yield('css')
    <link href="{{ asset('css/cms.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container-fluid container-cms">
                <a class="navbar-brand" href="{{ url('/') }}">
                    Somos REMO
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    @guest
                    @else
                        @yield('site')
                        <ul class="navbar-nav mr-auto nav-pills">
                            <li class="nav-item">
                                <a class="nav-link @if($site === 'home') active @endif" href="{{ route('admin') }}"><i class="fas fa-laptop-house"></i> Inicio</a>
                            </li>
                            @if(Auth::user()->isRoot())
                                <li class="nav-item">
                                    <a class="nav-link @if($site === 'users') active @endif" href="{{ route('users.index') }}"><i class="fas fa-users"></i> Usuarios</a>
                                </li>
                            @endif
                            <li class="nav-item">
                                <a class="nav-link @if($site === 'categories') active @endif" href="{{ route('categories.index') }}"><i class="fas fa-tags"></i> Procesos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if($site === 'types') active @endif" href="{{ route('types.index') }}"><i class="fas fa-list"></i> Tipos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if($site === 'forms') active @endif" href="{{ route('forms.index') }}"><i class="fas fa-clipboard-list"></i> Documentos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if($site === 'aspects') active @endif" href="{{ route('aspects.index') }}"><i class="far fa-list-alt"></i> Valores</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if($site === 'beginings') active @endif" href="{{ route('beginings.index') }}"><i class="far fa-list-alt"></i> Principios</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if($site === 'votes') active @endif" href="{{ route('votes.index') }}"><i class="fas fa-person-booth"></i> C. Excelencia</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if($site === 'banners') active @endif" href="{{ route('banners.index') }}"><i class="fas fa-images"></i> Banners</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if($site === 'polls') active @endif" href="{{ route('polls.index') }}"><i class="fas fa-poll"></i> Encuestas</a>
                            </li>
                        </ul>
                    @endguest

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <main class="main-container">
        @yield('content')
    </main>
</div>
<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/89d4349a78.js" crossorigin="anonymous"></script>
@yield('scripts')
</body>
</html>
