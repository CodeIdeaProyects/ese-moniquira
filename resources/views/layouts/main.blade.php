<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>ESE Moniquirá</title>

		<!-- Fonts -->
		<link rel="dns-prefetch" href="//fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Red+Rose:wght@300;400;700&display=swap" rel="stylesheet">

		<!-- Styles -->
		<link rel="shortcut icon" type="image/png" href="{{ asset('img/logo_ico.png') }}"/>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
		<link href="{{ asset('css/main.css') }}" rel="stylesheet">
		<link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
		@yield('css')
	</head>
	<body>
		<div id="menu">
			<div class="bg-menu">
				<div class="container-menu">
					<div class="logo">
						<a href="{{ route('index') }}" class="link-logo">Hospital regional de Moniquirá</a>
					</div>
					<div class="items">
						<a href="#" id="pull">
							<i class="fas fa-bars"></i>
						</a>
				  	    @guest
			  	            <a class="sesion-item-movil" href="{{ route('login') }}">Iniciar sesión</a>
				  	    @else
			  	            <a class="sesion-item-movil" href="#" id="btn-sesion">
			  	            	{{ Auth::user()->name }} <i class="fas fa-angle-down" id="arrow"></i>
			  	            </a>
				  	    @endguest
						<nav>
							@yield('menu')
							<a class="menu-item item-movil" href="{{ url('/') }}">Inicio</a>
							@if($site === 'index')
								<a class="menu-item salto" href="#services">Servicios</a>
							@else
				      			<a class="menu-item" href="{{ url('/') }}#services">Servicios</a>
							@endif
				      		<a class="menu-item" href="{{ route('we') }}">Nosotros</a>
				      		<a class="menu-item intranet" id="btn-novedades" href="#">Novedades</a>
				      		<a class="menu-item intranet" href="{{ route('home') }}">Intranet</a>
					  	    @guest
				  	            <a class="sesion-item" href="{{ route('login') }}">Iniciar sesión</a>
					  	    @else
				  	            <a class="sesion-item" href="#" id="btn-sesion-2">
				  	            	{{ Auth::user()->name }} <i class="fas fa-angle-down" id="arrow"></i>
				  	            </a>
					  	    @endguest
						</nav>
					</div>
				</div>
			</div>
			<div class="options-novedades" id="container-novedades">
				@if($categories->count() > 0)
					@foreach($categories->last()->categories as $category)
						<a class="link-option-sesion" href="{{ route('categories.show.subprocess', [$categories->last()->slug, $category->slug]) }}">
						    {{ $category->name }}
						</a>
					@endforeach
				@endif
            	{{-- <a class="link-option-sesion" href="{{ route('news', 'tecnovigilancia') }}">
            	    Sistema tecnovigilancia
            	</a>
            	<a class="link-option-sesion" href="{{ route('news', 'farmacovigilancia') }}">
            	    Sistema farmacovigilancia
            	</a>
            	<a class="link-option-sesion" href="{{ route('news', 'hemovigilancia') }}">
            	    Sistema hemovigilancia
            	</a>
            	<a class="link-option-sesion" href="{{ route('news', 'reactivo-vigilancia') }}">
            	    Sisema reactivo vigilancia
            	</a>
            	<a class="link-option-sesion" href="https://eseclínica_del_trabajador.com/public/user/macroprocesos/tablero-control-de-indicadores">
            	    Tablero control de indicadores
            	</a>
            	<a class="link-option-sesion" href="{{ route('news', 'derechos-deberes') }}">
            	    Derechos y deberes
            	</a>
            	<a class="link-option-sesion" href="{{ route('news', 'gestion-riesgo') }}">
            	    Gestión del riesgo
            	</a> --}}
			</div>
			<div class="options-sesion" id="container-sesion">
	            @guest
	            @else
	            	@if(Auth::user()->isRoot() || Auth::user()->isAdmin())
			            <a class="link-option-sesion" href="{{ route('admin') }}">
			                Panel administrativo
			            </a>
	            	@endif
	            	<a class="link-option-sesion" href="{{ route('users.change') }}">
	            	    Cambiar contraseña
	            	</a>
	            @endguest
	            <a class="link-option-sesion" href="{{ route('logout') }}"
	            onclick="event.preventDefault();
	            document.getElementById('logout-form').submit();">
	                Cerrar sesión
	            </a>
	            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                @csrf
	            </form>
			</div>
		</div>
		<div class="body">
			@yield('content')
		</div>
		<footer>
			<div class="footer-container">
				{{-- <div class="row">

				<hr class="hr-footer"> --}}
				<div class="row">
					<div class="col-md-6 text-center">
						<div class="container-info-footer">
							<h2>Hospital regional de Moniquirá</h2>
							Calle 4A No 9-101 Barrio Ricaurte Moniquirá, Boyacá
						</div>
						<img src="{{ asset('img/logo_blanco.png') }}" class="logo-footer">
					</div>
					<div class="col-md-3">
						<div class="container-info-footer">
							<h2>Contacto</h2>
							<ul class="list-social">
								<li><div class="div-social"><i class="fas fa-phone"></i> Tel.: 60(8)7282854</div></li>
								<li><div class="div-social"><i class="fas fa-phone"></i> Cel: (+57)311 519 66 47</div></li>
								<li><div class="div-social"><i class="fas fa-phone"></i> Cel: (+57)311 523 61 33</div></li>
								<li><div class="div-social"><i class="fas fa-phone"></i> Cel: (+57)310 732 27 54</div></li>
								<li><div class="div-social"><i class="far fa-envelope"></i>ventanillaunica@hrm.gov.co</div></li>
							</ul>
						</div>
					</div>
					<div class="col-md-3">
						<div class="container-info-footer">
							<h2>Síguenos</h2>
							<ul class="list-social">
								<li><div class="div-social"><i class="fab fa-whatsapp-square"></i> <a href="https://wa.me/573124316010" target="_blank">Cel: (+57)3124316010</a></div></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<div class="info-design">
			<a href="https://inaficsas.com" class="link-inafic" target="_blank"><img src="{{ asset('img/logo_inafic.png') }}"></a> | &copy; Todos los derechos reservados | Desarrollado por <strong>CodeIdea</strong>
		</div>
		{{-- Scripts --}}
		<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
		<script src="https://kit.fontawesome.com/89d4349a78.js" crossorigin="anonymous"></script>
		<script type="text/javascript">
			$(document).ready(function(e){
				$("#btn-sesion").click(function(){
				    $("#container-sesion").animate({
				        height: 'toggle'
				    });
				    if($('#arrow').hasClass('fa-angle-down')){
				    	$('#arrow').removeClass('fa-angle-down');
				    	$('#arrow').addClass('fa-angle-up');
				    }else{
				    	$('#arrow').removeClass('fa-angle-up');
				    	$('#arrow').addClass('fa-angle-down');
				    }
				});

				$("#btn-sesion-2").click(function(){
				    $("#container-sesion").animate({
				        height: 'toggle'
				    });
				    if($('#arrow').hasClass('fa-angle-down')){
				    	$('#arrow').removeClass('fa-angle-down');
				    	$('#arrow').addClass('fa-angle-up');
				    }else{
				    	$('#arrow').removeClass('fa-angle-up');
				    	$('#arrow').addClass('fa-angle-down');
				    }
				});

				$("#btn-novedades").click(function(){
				    $("#container-novedades").animate({
				        height: 'toggle'
				    });
				});

				$('.salto').click(function(e){
				    e.preventDefault();
				    var strAncla = $(this).attr('href');
				    $('body,html').stop(true,true).animate({
				        scrollTop: ($(strAncla).offset().top - 70)
				    },1000);
				});
			});

			$(function(){
				var pull = $('#pull');
				menu = $('nav');

				$(pull).on('click', function(e){
					e.preventDefault();
					menu.slideToggle();
				});
			});
		</script>
		@yield('scripts')
	</body>
</html>
