@extends('layouts.main')

@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
@endsection

@section('menu')
    <div class="d-none">
        {!! $site = 'index' !!}
    </div>
@endsection

@section('content')
    <div id="banner">
        <div id="fade">
            @foreach($banners as $banner)
                <div class="banner" style="background-image: url({{ asset('storage/banners') }}/{{ $banner->image }}">
                	@if(!is_null($banner->text))
                		<div class="text-banner">
                			<div>
                				{{ $banner->text }}
                			</div>
                		</div>
                	@endif
                	@if(!is_null($banner->link))
                		<a href="{{ $banner->link }}" class="link-banner" target="_blank"></a>
                	@endif
                </div>
            @endforeach
        </div>
        <button class="slick_banner" id="prev_banner"><i class="fas fa-angle-double-left"></i></button>
        <button class="slick_banner" id="next_banner"><i class="fas fa-angle-double-right"></i></button>
        <div id="container-marker-banner">
        </div>
    </div>
    <div>
    	<section class="section section-buttons" id="services">
    		<div class="buttons-container">
	    		<div class="row">
	    			<div class="@if(!is_null($vote)) col-md-3 @else col-md-4 @endif">
	    				<a href="{{ route('home') }}" class="link-btn"><img src="{{ asset('img/intranet.png') }}" class="img-btn"></a>
	    			</div>
	    			<div class="@if(!is_null($vote)) col-md-3 @else col-md-4 @endif">
	    				<a href="{{ url('/') }}/moodle" class="link-btn" target="_blank"><img src="{{ asset('img/aula_virtual.png') }}" class="img-btn"></a>
	    			</div>
	    			@if(!is_null($vote))
		    			<div class="col-md-3">
		    				<a href="{{ route('qualifications.showForm', $vote->id) }}" class="link-btn"><img src="{{ asset('img/funcionario_integral.png') }}" class="img-btn"></a>
		    			</div>
	    			@endif
	    			<div class="@if(!is_null($vote)) col-md-3 @else col-md-4 @endif">
	    				<a href="{{ route('polls.show.user') }}" class="link-btn"><img src="{{ asset('img/encuesta.png') }}" class="img-btn"></a>
	    			</div>
	    		</div>
    		</div>
    	</section>
    	{{-- <section class="section">
    		<div class="contact-container">
    			<div class="row col-video">
	    			<div class="col-md-6 contact-info">
						Brindamos servicios de salud oportunos y de alta calidad, mediante la atención especializada de los accidentes de tránsito, trabajo y escolares, además de la prevención y el tratamiento integral de las enfermedades profesionales.<br>
                        Ubicados estratégicamente sobre la Avenida Calle 161 en la esquina de la carrera 16D, frente a Coomeva EPS y sobre la ruta hacia la Fundación Cardio Infantil.<br>
                        Contamos con personal técnico que maneja altos estándares de calidad en apoyo de los servicios de salud
	    			</div>
	    			<div class="col-md-6 container-video">
	    				<video id="video" autoplay muted>
	    					<source src="{{ asset('video/video.mp4') }}" type="video/mp4">
	    				</video>
	    				<div class="pause" onclick="playPause()">
	    					<i class="fas fa-pause" id="icon_play"></i>
	    				</div>
	    			</div>
    			</div>
    		</div>
    	</section> --}}
    	<section class="section-form">
    		<div class="form-container">
    			<div class="container-fluid">
    				<div class="row">
    					<div class="col-md-6">

    					</div>
    					<div class="col-md-6">
                            <div class="text-contact">
                                <div class="title-contact">Reporte de sucesos de seguridad del paciente</div>
                            </div>
    						<form action="{{ route('contacto.mail') }}" method="POST">
    							@csrf
    							<div class="row row-contact">
    							    <div class="col-md-6">
    							        <input type="text" name="sede" class="form-control input-contact" required placeholder="Sede que reporta">
    							    </div>
    							    <div class="col-md-6">
    							        <input type="date" name="fecha" class="form-control input-contact" required placeholder="Fecha de ocurrencia del suceso">
    							    </div>
    							</div>
    							<div class="row row-contact">
    							    <div class="col-md-12">
    							        <input type="text" name="nombre" class="form-control input-contact" required placeholder="Nombre de quien reporta">
    							    </div>
    							</div>
    							<div class="row row-contact">
    							    <div class="col-md-12">
    							        <textarea placeholder="Descripción del evento
(Describa la fecha en que se presento el suceso, el nombre del paciente, el numero de identificación, suceso presentado, entre otros aspectos que considere relevantes para el análisis de incidentes de seguridad del paciente)" name="descripcion" class="form-control input-contact" required rows="6"></textarea>
    							    </div>
    							</div>
                                <div class="row row-contact">
                                    <div class="col-md-12">
                                        <textarea placeholder="Falla detectada
(Describa brevemente cuales considera que fueron las fallas que que ocasionaron el suceso)" name="falla" class="form-control input-contact" required></textarea>
                                    </div>
                                </div>
    							<div class="row row-contact">
    							    <div class="col-md-12">
    							        <button type="submit" class="btn btn-form">Enviar</button>
    							    </div>
    							</div>
    						</form>
    					</div>
    				</div>
    			</div>
    		</div>
    	</section>
    </div>
@endsection

@section('scripts')
	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	<script type="text/javascript">
		// Función de movimiento de banners
		$("#fade").slick({
		    infinite:true,
		    fade:true,
		    autoplay:true,
		    autoplaySpeed:7000,
		    cssEase:"linear",
		    prevArrow: $('#prev_banner'),
		    nextArrow: $('#next_banner'),
		    dots: true,
		    dotsClass: 'marker-banner',
		    appendDots: $('#container-marker-banner'),
		});
		// Fin función de movimiento de banners

		var myVideo = document.getElementById("video");
		function playPause() {
			if (myVideo.paused){
		    	myVideo.play();
		    	$('#icon_play').removeClass('fa-play');
		    	$('#icon_play').addClass('fa-pause');
			}else{
		    	myVideo.pause();
		    	$('#icon_play').removeClass('fa-pause');
		    	$('#icon_play').addClass('fa-play');
			}
		}
	</script>
@endsection
