<!DOCTYPE html>
<html>
	<head>
		<title>Funcionario excelencia</title>
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,300;0,400;0,500;0,700;1,100;1,200;1,300;1,400;1,700&display=swap" rel="stylesheet"> 
	</head>
	<body>
		<div style="background-color: #E5E5E5; width: 80%; margin: auto; margin-top: 100px; text-align: center; border: 1px solid #989898; padding: 0px 30px; font-family: 'Montserrat', sans-serif;">
			<img src="{{ asset('img/f_excelencia.png') }}" style="width: 100%; margin: auto;">
			<div style="background-color: #FFFFFF; padding: 30px;">
				<h1>¡Felicitaciones {{ $nombre }}!</h1>
				<p>
					Has sido seleccionado por tu compañero <strong>{{ $nombre2 }}</strong> reconoconociendo el valor <strong>{{ $valor }}</strong> y el principio <strong>{{ $principio }}</strong>
				</p>
				<div style="font-style: italic;">
					“{!! $justificacion !!}”
				</div>
				<hr style="border-color: #989898;">
				<h3>Recuerda</h3>
				<p>Te conviertes en Funcionario de Excelencia cuando tus compañeros te han enviado varias nominaciones reconociendo los valores, el trato humanizado en tus actitudes y/o las aptitudes con las cuales haces tus labores diarias. Si recolectas muchas nominaciones recibirás un reconocimiento especial en un evento importante para el hospital</p>
			</div>
		</div>
	</body>
</html>